//import {Point, Bullet, Enemy, MovingObject} from "/javascript/classes";

var tank;
var bullet;
var ActionObjects;

var restart = false;
var paused = false;
var enemyAdderTimer = 1;

var forward = false,
	backward = false,
	left = false,
	right = false;

var PlayerScore = 0;
var BossCD = 0;
var BossTime = 5;

var timer;

function distance(point1, point2)
{
	return Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
}
function getRandomColor() 
{
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) 
	  color += letters[Math.floor(Math.random() * 16)];
	return color;
}
function loadPage()
{
	window.addEventListener('keydown', keysPressed);
	window.addEventListener('keyup',  keysReleased);
	window.addEventListener('mousemove',  mouseMoving);
	window.addEventListener('click',  mouseClicking);
	
	loadGame();
}
function loadGame()
{
	restart = false;
	paused = false;
	PlayerScore = 0;
	AIScore = 0;
	EnemyAdderTimer = 1;

	tank = new MovingObject(new Point(100.0, 100.0), 0.0, "red");
	ActionObjects = [tank,
		new Enemy(new Point(Math.random() * 1300, -30, 0.0, getRandomColor()))];
	bullet = [];
	draw();

	timer = setInterval(updateScene, 30);
}
function loosing()
{
	clearTimeout(timer);
	restart = true;
}
function addEnemy()
{
	enemyAdderTimer = 0;


	var noPlace = true;
	for(var i = 1; i < ActionObjects.length; i++)
	if(ActionObjects[i] == null)
	{
		if(BossCD == BossTime)
			ActionObjects[i] = 
				new EnemySniper(new Point(Math.random() * 1300, -30), 0.0, getRandomColor());
		else
			ActionObjects[i] = 
				new Enemy(new Point(Math.random() * 1300, -30), 0.0, getRandomColor());
		noPlace = false;
		break;
	}
	if(noPlace)
	{
		if(BossCD == BossTime)
			ActionObjects[ActionObjects.length] = 
			new EnemySniper(new Point(Math.random() * 1300, -30), 0.0, getRandomColor());
		else
			ActionObjects[ActionObjects.length] = 
			new Enemy(new Point(Math.random() * 1300, -30), 0.0, getRandomColor());
	}
	if(BossCD == BossTime)
		BossCD = 0;
	else
		BossCD += 1;

}

function mouseClicking(e)
{
	addBullet(new Bullet(this.tank.getMuzzle(), this.tank.muzzleAngle));
	var audio = new Audio("assets/Laser_Shoot1.mp3");
	audio.play();
}
function addBullet(bull)
{
	if(bullet.length == 0)
		bullet[0] = bull;
	else
	{
		var isNull = false;
		for(var i = 0; i < bullet.length; i++)
		{ 
			if(bullet[i] == null)
			{
				bullet[i] = bull;
				isNull = true;
				break;
			}
		}
		if(!isNull)
		{
			bullet[bullet.length] = bull;
		}
	}
}
function mouseMoving(e)
{
	tank.updateMuzzleAngle(new Point(e.clientX, e.clientY));
}
function keysPressed(e)
{
	if(e.key == 'w')
	{
		forward = true;
		backward = false;
	}
	else if(e.key == 's')
	{
		forward = false;
		backward = true;
	}
	else if(e.key == 'a')
	{
		left = true;
		right = false;
	}
	else if(e.key == 'd')
	{
		left = false;
		right = true;
	}

}

function keysReleased(e)
{
	if(e.key == 'w')
	{
		forward = false;
	}
	if(e.key == 's')
	{
		backward =false;
	}
	if(e.key == 'a')
	{
		left = false;
	}
	if(e.key == 'd')
	{
		right = false;
	}
	if(e.key == ' ')
	{
		if(restart)
			loadGame();
		else
		{
			paused = !paused;
		}
	}

}

function updateScene()
{
	if(!paused)
	{
		UpdateTank();
		UpdateBullets();
		UpdateEnemies();

		enemyAdderTimer++;
		if(enemyAdderTimer == 300)
			addEnemy();
	}

	draw();
}

function UpdateTank()
{
	if(left || right)
	tank.rotate(left);

	if(forward || backward)
		tank.move(forward);
}
function UpdateBullets()
{
	for(var i = 0, j; i < bullet.length; i++)
	{
		if(bullet[i] != null)
		{
			if( bullet[i].isAlive)
			{
				for(j = 0; j < ActionObjects.length; j++)
				if(ActionObjects[j] != null)
				{
					if(distance(bullet[i].loc, ActionObjects[j].loc) 
					<= bullet[i].size + ActionObjects[j].size)
					{
						bullet[i].detonate();
						ActionObjects[j].health--;
						if(ActionObjects[j].health < 0)
						{
							if(j != 0)
							{
								ActionObjects[j] = null;
								PlayerScore += 10;
								tank.health += 5;
								if(tank.health > 100)
								tank.health = 100;
							}
							else
								loosing();
						}
					}
				}
				bullet[i].update();
			}
			else
				bullet[i] = null;
		}
	}
}
function UpdateEnemies()
{
	for(var i = 1; i < ActionObjects.length; i++)
	{
		if(ActionObjects[i] != null)
		{
			ActionObjects[i].update(tank.loc);
			if(ActionObjects[i].isReadyToFire())
				addBullet(ActionObjects[i].createBullet());
		}
	}
}

function draw() 
{
	var canvas = document.getElementById('DrawingArea');
	if (canvas.getContext) 
	{
		var context = canvas.getContext('2d');
		if(restart)
		{
			context.fillStyle = "100px Arial";
			context.fillText("YOU DIED", 600, 200); 
		}
		else if(paused)
		{
			context.fillStyle = "100px Arial";
			context.fillText("PAUSE", 600, 200); 
		}
		else
		{
			context.clearRect(0, 0, canvas.width, canvas.height);

			for(var i = 0; i < bullet.length; i++)
				if(bullet[i] != null && bullet[i].isAlive)
					bullet[i].draw(context);
			
			for(var i = 0; i < ActionObjects.length; i++)
			if(ActionObjects[i] != null)
				ActionObjects[i].draw(context);

			context.fillText("Your score: " + PlayerScore, 20, 25);

			context.fillText("HP: " + tank.health + "%", 20, 50);
		}
	}
}







class Point
{
	constructor(x, y)
	{
		this.x = x;
		this.y = y;
	}
}

class SimpleObject
{
	constructor(loc, size, color)
	{
		this.loc = new Point(0.0, 0.0);
		this.loc.x = loc.x;
		this.loc.y = loc.y;
		this.size = size;
		this.color = color;
		this.muzzleLength = 0;
		this.health = 100;
	}
	draw(contex)
	{

	}

	getMuzzle()
	{
		return new Point(this.loc.x + this.muzzleLength * Math.sin(this.muzzleAngle), this.loc.y - this.muzzleLength * Math.cos(this.muzzleAngle));
	}
}

class Bullet extends SimpleObject
{
	constructor(loc, angle)
	{
		super(loc, 2.0, "white");
		this.angle = angle;
		this.expRadius = 15;
		
		this.speed = 6.0;
		this.lifeSpan = 150;
		this.counter = 0;
		this.isAlive = true;
		this.detonated = false;
	}
	detonate()
	{
		this.counter = this.lifeSpan;
		this.detonated = true;
	}
	update()
	{
		if(this.isAlive)
		{
			if(this.counter != this.lifeSpan)
			{
				this.loc.x += this.speed * Math.sin(this.angle);
				this.loc.y -= this.speed * Math.cos(this.angle);
				
				this.counter += 1;
			}
			else
			{
				if(this.size < this.expRadius)
				{
					this.size+=4;
				}
				else
					this.isAlive = false;
			}
		}
	}

	draw(contex)
	{
		contex.fillStyle = this.color;

		contex.beginPath();			
		contex.arc(this.loc.x, this.loc.y, this.size, 0, 2 * Math.PI);
		contex.fill();
		contex.fillStyle = "black";
		contex.stroke();
	}
}
class SuperBullet extends Bullet
{
	constructor(loc, angle)
	{
		super(loc, 2.0, "white");
		this.angle = angle;
		this.expRadius = 100;
		
		this.speed = 5.0;
		this.lifeSpan = 150;
		this.counter = 0;
		this.isAlive = true;
		this.detonated = false;
	}
	update()
	{
		if(this.isAlive)
		{
			if(this.counter != this.lifeSpan)
			{
				this.loc.x += this.speed * Math.sin(this.angle);
				this.loc.y -= this.speed * Math.cos(this.angle);
				
				this.counter += 1;
				this.size += 0.5;
			}
			else
			{
				if(this.size < this.expRadius)
				{
					this.size+=4;
				}
				else
					this.isAlive = false;
			}
		}
	}
}

class Laser extends Bullet
{
	constructor(loc, angle)
	{
		super(loc, 2.0, "white");
		this.angle = angle;
		this.expRadius = 100;
		
		this.speed = 5.0;
		this.lifeSpan = 150;
		this.counter = 0;
		this.isAlive = true;
		this.detonated = false;
	}
	update()
	{
		if(this.isAlive)
		{
			if(this.counter != this.lifeSpan)
			{
				this.loc.x += this.speed * Math.sin(this.angle);
				this.loc.y -= this.speed * Math.cos(this.angle);
				
				this.counter += 1;
				this.size += 0.5;
			}
			else
			{
				if(this.size < this.expRadius)
				{
					this.size+=4;
				}
				else
					this.isAlive = false;
			}
		}
	}
}

class Enemy extends SimpleObject
{
	constructor(loc, angle, color)
	{
		super(loc, 16, color);
		this.color = "pink";
		this.cooldown = 20;
		this.counter = 0;
		this.angle = angle;
		this.muzzleAngle = 0;
		this.isAlive = true;
		this.muzzleLength = this.size + 5;
		
		this.health = 40;
		this.score = 10;
		
		this.speed = 1.6;
		this.rotationSpeed = this.speed / 20.0;
	}
	updateMuzzleAngle(targetLoc)
	{
		this.muzzleAngle = Math.asin((targetLoc.x - this.loc.x) 
		/ Math.sqrt((this.loc.y - targetLoc.y)  * (this.loc.y - targetLoc.y) 
			+ (targetLoc.x - this.loc.x) * (targetLoc.x - this.loc.x)));

		if(this.counter == this.cooldown)
		{
			this.counter = 0;
		}
	}
	rotate(targetLoc)
	{
		if(targetLoc.y > this.loc.y)
		{
			this.muzzleAngle *= -1;
			this.muzzleAngle += Math.PI;
		}
		if(this.muzzleAngle > this.angle + this.rotationSpeed)
			this.angle += this.rotationSpeed;
		else if(this.muzzleAngle< this.angle - this.rotationSpeed)
			this.angle -= this.rotationSpeed;
	}
	move()
	{
		this.loc.x += this.speed * Math.sin(this.angle);
		this.loc.y -= this.speed * Math.cos(this.angle);
	}
	update(targetLoc)
	{
		this.updateMuzzleAngle(targetLoc);
		this.rotate(targetLoc);
		this.move();

		this.counter += 1;
		if(this.counter == this.cooldown)
			this.counter = 0;
	}
	isReadyToFire()
	{
		return this.counter == 0;
	}
	createBullet()
	{
		return new Bullet(this.getMuzzle(), this.muzzleAngle);
	}

	draw(ctx)
	{
		ctx.beginPath();
		ctx.moveTo(this.loc.x, this.loc.y);
		ctx.lineTo(this.getMuzzle().x, this.getMuzzle().y);
		ctx.stroke();

		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.arc(this.loc.x, this.loc.y, 
			this.size / 2, 0, 2 * Math.PI);
		ctx.fill();
		ctx.fillStyle = "black";
		ctx.stroke();
	}
}

class EnemySniper extends Enemy
{
	constructor(loc, angle, color)
	{
		super(loc, 0, color);
		this.color = "blue";
		this.speed = 1;
		this.size = 18;
		this.cooldown = 100;
		this.counter = 0;
		this.angle = angle;
		this.muzzleAngle = 0;
		this.isAlive = true;
		this.muzzleLength = this.size + 10;
		
		this.health = 200;
		this.score = 10;
		
		this.speed = 1.6;
		this.rotationSpeed = this.speed / 20.0;
	}
	createBullet()
	{
		return new SuperBullet(this.getMuzzle(), this.muzzleAngle);
	}
}
class MovingObject extends SimpleObject
{
	constructor(loc, angle, color)
	{
		super(loc, 10, color);
		this.angle = angle;

		this.muzzleAngle = 0.0;
		this.lookAt = new Point(0.0, 0.0);
		
		this.speed = 2.6;
		this.rotationSpeed = this.speed / 50.0;
		this.muzzleLength = 20;
		
		this.Points = [new Point(this.loc.x, this.loc.y - 10), 
			new Point(this.loc.x - 10, this.loc.y + 10), 
			new Point(this.loc.x, this.loc.y + 20), 
			new Point(this.loc.x + 10, this.loc.y + 10)];

		this.setLocation();	
	}
	
	updateMuzzleAngle(e)
	{
		this.lookAt = e;
		this.muzzleAngle = Math.asin((e.x - this.loc.x) 
		/ Math.sqrt((this.loc.y - e.y)  * (this.loc.y - e.y)  + (e.x - this.loc.x) * (e.x - this.loc.x)));
		if(e.y > this.loc.y)
		{
			this.muzzleAngle *= -1;
			this.muzzleAngle += Math.PI;
		}
	}

	setLocation()
	{
		this.loc =new Point(0.0, 0.0);
		for(var i =0; i< this.Points.length; i++)
		{
			this.loc.x += this.Points[i].x / this.Points.length;
			this.loc.y += this.Points[i].y / this.Points.length;
		}
	}
	move(direction)
	{
		for( var i =0; i< this.Points.length; i++)
		{

			if(direction == true)
			{
				this.Points[i].x += this.speed* Math.sin(this.angle);
				this.Points[i].y -= this.speed * Math.cos(this.angle);
			}
			else
			{
				this.Points[i].x -= this.speed* Math.sin(this.angle);
				this.Points[i].y += this.speed * Math.cos(this.angle);
			}
		}
		this.setLocation();
		
		this.updateMuzzleAngle(this.lookAt);
	}
	
	rotate(direction)
	{
		
		var p = new Point(0.0, 0.0);
		for(var i =0; i< this.Points.length; i++)
		{
			p.x = this.Points[i].x;
			p.y = this.Points[i].y;
			
			if(direction == true)
			{
				this.Points[i].x = (p.x - this.loc.x) * Math.cos(this.rotationSpeed) 
				+ (p.y - this.loc.y) * Math.sin(this.rotationSpeed) + this.loc.x;
				this.Points[i].y = -(p.x - this.loc.x) * Math.sin(this.rotationSpeed) 
				+ (p.y - this.loc.y) * Math.cos(this.rotationSpeed) + this.loc.y;
			}
			else
			{
				this.Points[i].x = (p.x - this.loc.x) * Math.cos(this.rotationSpeed) 
				- (p.y - this.loc.y) * Math.sin(this.rotationSpeed) + this.loc.x;
				this.Points[i].y = (p.x - this.loc.x) * Math.sin(this.rotationSpeed) 
				+ (p.y - this.loc.y) * Math.cos(this.rotationSpeed) + this.loc.y;
			}
		}
		
		if(direction == false)
			this.angle += this.rotationSpeed;
		else
			this.angle -= this.rotationSpeed;
	}
	draw(ctx)
	{
		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.moveTo(this.Points[0].x, this.Points[0].y);
		for(var i = 1; i < this.Points.length; i++)
			ctx.lineTo(this.Points[i].x, this.Points[i].y);
		ctx.lineTo(this.Points[0].x, this.Points[0].y);
		
		ctx.fill();
		ctx.fillStyle = "black";
		ctx.stroke();
		
		ctx.beginPath();
		ctx.arc(this.loc.x, this.loc.y, 5, 0, 2 * Math.PI);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.moveTo(this.loc.x, this.loc.y);
		ctx.lineTo(this.getMuzzle().x, this.getMuzzle().y);
		ctx.stroke();

		ctx.font = "12px Arial";
		ctx.fillText("Champignon_9000", this.loc.x - 50, this.loc.y - 30); 
	}
}
