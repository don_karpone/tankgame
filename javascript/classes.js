export {Point, SimpleObject, Bullet, Enemy, MovingObject}

class Point
{
	constructor(x, y)
	{
		this.x = x;
		this.y = y;
	}
}

class SimpleObject
{
	constructor(loc, size, color)
	{
		this.loc = new Point(0.0, 0.0);
		this.loc.x = loc.x;
		this.loc.y = loc.y;
		this.size = size;
		this.color = color;
		this.muzzleLength = 0;
		this.health = 100;
	}
	draw(contex)
	{

	}

	getMuzzle()
	{
		return new Point(this.loc.x + this.muzzleLength * Math.sin(this.muzzleAngle), this.loc.y - this.muzzleLength * Math.cos(this.muzzleAngle));
	}
}
class Bullet extends SimpleObject
{
	constructor(loc, angle)
	{
		super(loc, 2.0, "white");
		this.angle = angle;
		this.expRadius = 15;
		
		this.speed = 6.0;
		this.lifeSpan = 150;
		this.counter = 0;
		this.isAlive = true;
		this.detonated = false;
	}
	detonate()
	{
		this.counter = this.lifeSpan;
		this.detonated = true;
	}
	update()
	{
		if(this.isAlive)
		{
			if(this.counter != this.lifeSpan)
			{
				this.loc.x += this.speed * Math.sin(this.angle);
				this.loc.y -= this.speed * Math.cos(this.angle);
				
				this.counter += 1;
			}
			else
			{
				if(this.size < this.expRadius)
				{
					this.size+=4;
				}
				else
					this.isAlive = false;
			}
		}
	}

	draw(contex)
	{
		contex.fillStyle = this.color;

		contex.beginPath();			
		contex.arc(this.loc.x, this.loc.y, this.size, 0, 2 * Math.PI);
		contex.fill();
		contex.fillStyle = "black";
		contex.stroke();
	}
}
class Enemy extends SimpleObject
{
	constructor(loc, angle, color)
	{
		super(loc, 16, color);
		this.cooldown = 20;
		this.counter = 0;
		this.angle = angle;
		this.muzzleAngle = 0;
		this.isAlive = true;
		this.muzzleLength = 20;
		
		this.speed = 1.6;
		this.rotationSpeed = this.speed / 20.0;
	}
	updateMuzzleAngle(targetLoc)
	{
		this.muzzleAngle = Math.asin((targetLoc.x - this.loc.x) 
		/ Math.sqrt((this.loc.y - targetLoc.y)  * (this.loc.y - targetLoc.y) 
			+ (targetLoc.x - this.loc.x) * (targetLoc.x - this.loc.x)));

		if(this.counter == this.cooldown)
		{
			this.counter = 0;
		}
	}
	rotate(targetLoc)
	{
		if(targetLoc.y > this.loc.y)
		{
			this.muzzleAngle *= -1;
			this.muzzleAngle += Math.PI;
		}
		if(this.muzzleAngle > this.angle + this.rotationSpeed)
			this.angle += this.rotationSpeed;
		else if(this.muzzleAngle< this.angle - this.rotationSpeed)
			this.angle -= this.rotationSpeed;
	}
	move()
	{
		this.loc.x += this.speed * Math.sin(this.angle);
		this.loc.y -= this.speed * Math.cos(this.angle);
	}
	update(targetLoc)
	{
		this.updateMuzzleAngle(targetLoc);
		this.rotate(targetLoc);
		this.move();

		this.counter += 1;
		if(this.counter == this.cooldown)
			this.counter = 0;
	}
	isReadyToFire()
	{
		return this.counter == 0;
	}

	draw(ctx)
	{
		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.arc(this.loc.x, this.loc.y, 
			this.size / 2, 0, 2 * Math.PI);
		ctx.fill();
		ctx.fillStyle = "black";
		ctx.stroke();

		ctx.beginPath();
		ctx.moveTo(this.loc.x, this.loc.y);
		ctx.lineTo(this.getMuzzle().x, this.getMuzzle().y);
		ctx.stroke();
	}
}
class MovingObject extends SimpleObject
{
	constructor(loc, angle, color)
	{
		super(loc, 10, color);
		this.angle = angle;

		this.muzzleAngle = 0.0;
		this.lookAt = new Point(0.0, 0.0);
		
		this.speed = 2.2;
		this.rotationSpeed = this.speed / 50.0;
		this.muzzleLength = 20;
		
		this.Points = [new Point(this.loc.x, this.loc.y - 10), 
			new Point(this.loc.x - 10, this.loc.y + 10), 
			new Point(this.loc.x, this.loc.y + 20), 
			new Point(this.loc.x + 10, this.loc.y + 10)];

		this.setLocation();	
	}
	
	updateMuzzleAngle(e)
	{
		this.lookAt = e;
		this.muzzleAngle = Math.asin((e.x - this.loc.x) 
		/ Math.sqrt((this.loc.y - e.y)  * (this.loc.y - e.y)  + (e.x - this.loc.x) * (e.x - this.loc.x)));
		if(e.y > this.loc.y)
		{
			this.muzzleAngle *= -1;
			this.muzzleAngle += Math.PI;
		}
	}

	setLocation()
	{
		this.loc =new Point(0.0, 0.0);
		for(var i =0; i< this.Points.length; i++)
		{
			this.loc.x += this.Points[i].x / this.Points.length;
			this.loc.y += this.Points[i].y / this.Points.length;
		}
	}
	move(direction)
	{
		for( var i =0; i< this.Points.length; i++)
		{

			if(direction == true)
			{
				this.Points[i].x += this.speed* Math.sin(this.angle);
				this.Points[i].y -= this.speed * Math.cos(this.angle);
			}
			else
			{
				this.Points[i].x -= this.speed* Math.sin(this.angle);
				this.Points[i].y += this.speed * Math.cos(this.angle);
			}
		}
		this.setLocation();
		
		this.updateMuzzleAngle(this.lookAt);
	}
	
	rotate(direction)
	{
		
		var p = new Point(0.0, 0.0);
		for(var i =0; i< this.Points.length; i++)
		{
			p.x = this.Points[i].x;
			p.y = this.Points[i].y;
			
			if(direction == true)
			{
				this.Points[i].x = (p.x - this.loc.x) * Math.cos(this.rotationSpeed) 
				+ (p.y - this.loc.y) * Math.sin(this.rotationSpeed) + this.loc.x;
				this.Points[i].y = -(p.x - this.loc.x) * Math.sin(this.rotationSpeed) 
				+ (p.y - this.loc.y) * Math.cos(this.rotationSpeed) + this.loc.y;
			}
			else
			{
				this.Points[i].x = (p.x - this.loc.x) * Math.cos(this.rotationSpeed) 
				- (p.y - this.loc.y) * Math.sin(this.rotationSpeed) + this.loc.x;
				this.Points[i].y = (p.x - this.loc.x) * Math.sin(this.rotationSpeed) 
				+ (p.y - this.loc.y) * Math.cos(this.rotationSpeed) + this.loc.y;
			}
		}
		
		if(direction == false)
			this.angle += this.rotationSpeed;
		else
			this.angle -= this.rotationSpeed;
	}
	draw(ctx)
	{
		ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.moveTo(this.Points[0].x, this.Points[0].y);
		for(var i = 1; i < this.Points.length; i++)
			ctx.lineTo(this.Points[i].x, this.Points[i].y);
		ctx.lineTo(this.Points[0].x, this.Points[0].y);
		
		ctx.fill();
		ctx.fillStyle = "black";
		ctx.stroke();
		
		ctx.beginPath();
		ctx.arc(this.loc.x, this.loc.y, 5, 0, 2 * Math.PI);
		ctx.stroke();
		
		ctx.beginPath();
		ctx.moveTo(this.loc.x, this.loc.y);
		ctx.lineTo(this.getMuzzle().x, this.getMuzzle().y);
		ctx.stroke();

		ctx.font = "12px Arial";
		ctx.fillText("Champignon_9000", this.loc.x - 50, this.loc.y - 30); 
	}
}